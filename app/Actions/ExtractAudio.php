<?php

namespace App\Actions;

use FFMpeg\Format\Audio\Mp3;
use FFMpeg\Format\Audio\Aac;
use FFMpeg\Coordinate\TimeCode;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
//use ProtoneMedia\LaravelFFMpeg\Format\Audio\Aac;
use ProtoneMedia\LaravelFFMpeg\Exporters\EncodingException;

class ExtractAudio
{

	public function execute(string $videoPath, $outputFileName)
	{
		$storagePath = Storage::disk('public')->path('media');
		$filename = $storagePath.'/'.$outputFileName.'.mp3';
		
		$ffmpeg = \FFMpeg\FFMpeg::create();
		$video = $ffmpeg->open($videoPath);
		
		// Set the formats
		$output_format = new \FFMpeg\Format\Audio\Mp3(); // Here you choose your output format
		$output_format->setAudioCodec("libmp3lame");

		$video->save($output_format, $filename);
		return url("storage/media/{$outputFileName}.mp3");
    		
		// //return FFMpeg::openUrl($videoPath)->export()->toDisk('public')->inFormat(new Aac)->save($filename);
		// return FFMpeg::openWithInputOptions($videoPath, ['i'])->export()->toDisk('public')->inFormat(new Aac)->save($filename);

		// } catch (EncodingException $exception) {

  		//   		$command = $exception->getCommand();
  		//   		$errorLog = $exception->getErrorOutput();
  		//   		dd($command);
  		//   		dd($errorLog);
		// }
	}
}
