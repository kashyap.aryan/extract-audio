<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use PHPHtmlParser\Dom;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Actions\ExtractAudio;

class ProcessMediaController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Extract Media
     */
    public function extractAudio()
    {    	
    	$category = request('category');

    	$url = "https://promo.com/templates/{$category}";
    	$response = Http::get($url);
    	
    	if($response->ok())
    	{
    		$responseBody = $response->body();
    		
    		$videos = $this->parseVideoMeta($responseBody);

    		$firstVideo = $videos->props->pageProps->videos[0];    		
    		$outputFilename = $firstVideo->id;

    		$slug = $firstVideo->slug;


    		$responseSlug = Http::get("https://promo.com/promoVideos/templates/get-by-slug?slug=".$slug);
    		
    		if($responseSlug->ok() )
    		{
    			$jsonResponse = json_decode($responseSlug->body());
    			//dd($jsonResponse->response->body->template->videos->wide->previewUrl);
    			$videoUrl = $jsonResponse->response->body->template->videos->wide->previewUrl;
	    		return [
	    			'status' => 'success',
	    			'id' => $firstVideo->id,
	    			'mp3' => (new ExtractAudio())->execute($videoUrl, $outputFilename)
	    		];

    		}


    	}
    	else{
    		return (['status' => 'failure']);
    	}
    }

    /**
     * Get MP3
     */
    public function getMp3($videoId)
    {
    	if(Storage::disk('public')->missing('media/'.$videoId.'.mp3'))
    	{
    		abort(404);
    	}
    	return Storage::disk('public')->download('media/'.$videoId.'.mp3');
    }

    protected function parseVideoMeta(string $responseBody)
    {
    	$match = [];
		preg_match('/<script id="__NEXT_DATA__" type="application\/json">(.*?)<\/script>/s', $responseBody, $match);
		return json_decode($match[1]);
	}
}
