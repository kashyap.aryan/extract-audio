<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProcessMediaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('promo2mp3/{category}', ProcessMediaController::class);
Route::post('promo2mp3', [ProcessMediaController::class, 'extractAudio'])->name('promo2mp3');
Route::get('mp3/{videoId}', [ProcessMediaController::class, 'getMp3'])->name('promo2mp3');